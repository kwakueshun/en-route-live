import Foundation

let IP = ""
let MOBILE_MONEY_PAYMENT_IP = "";
let MOBILE_MONEY_PAYMENT_API_RESPOSNE_IP = "";
let ACCOUNTS_FIREBASE_PASSWORDS = "";
let PAYMENT_API_TOKEN = "";

typealias callback = (_ success: Bool) -> ()

// Boolean auth UserDefaults keys
let DEFAULTS_REGISTERED = "isRegistered"
let DEFAULTS_AUTHENTICATED = "isAuthenticated"

// Auth Email
let DEFAULTS_EMAIL = "email"

// Auth Token
let DEFAULTS_TOKEN = "authToken"
let BASE_API_URL = "http://192.168.8.102:3005/v1"
let POST_REGISTER_ACCT = "\(BASE_API_URL)/account/register"
let POST_LOGIN_ACCT = "\(BASE_API_URL)/account/login"




