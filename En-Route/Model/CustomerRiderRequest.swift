import Foundation

open class CustomerRiderRequest: Codable {
    
    var ridertoken: String
    
    var customertoken: String
}

